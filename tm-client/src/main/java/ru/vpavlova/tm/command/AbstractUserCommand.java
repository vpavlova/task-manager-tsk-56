package ru.vpavlova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vpavlova.tm.endpoint.*;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    public AdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

}

