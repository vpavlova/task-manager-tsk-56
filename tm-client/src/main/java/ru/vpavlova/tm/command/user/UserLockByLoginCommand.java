package ru.vpavlova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.command.AbstractUserCommand;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.util.TerminalUtil;

@Component
public class UserLockByLoginCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-lock";
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Lock User:");
        System.out.println("Enter Login:");
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        @NotNull final String login = TerminalUtil.nextLine();
        adminEndpoint.lockUserByLogin(session, login);
    }

}
