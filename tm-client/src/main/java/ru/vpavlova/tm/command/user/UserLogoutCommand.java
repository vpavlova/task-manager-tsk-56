package ru.vpavlova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.command.AbstractUserCommand;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;

@Component
public class UserLogoutCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from program.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        sessionEndpoint.closeSession(session);
        sessionService.setSession(null);

    }

}
