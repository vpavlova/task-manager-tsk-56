package ru.vpavlova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.command.AbstractTaskCommand;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.endpoint.Task;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.entity.TaskNotFoundException;
import ru.vpavlova.tm.util.TerminalUtil;

import java.util.Optional;

@Component
public class TaskByIdViewCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = taskEndpoint.findTaskById(session, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

}
