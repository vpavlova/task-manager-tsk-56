package ru.vpavlova.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vpavlova.tm.api.service.ICommandService;
import ru.vpavlova.tm.endpoint.Role;
import ru.vpavlova.tm.service.SessionService;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    @Autowired
    protected SessionService sessionService;

    @Nullable
    @Autowired
    protected ICommandService commandService;

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

}
