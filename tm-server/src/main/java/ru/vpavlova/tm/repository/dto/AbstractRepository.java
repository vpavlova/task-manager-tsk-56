package ru.vpavlova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vpavlova.tm.api.repository.IRepository;
import ru.vpavlova.tm.dto.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @Autowired
    protected EntityManager entityManager;

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    @Override
    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    @NotNull
    @Override
    public Optional<E> getSingleResult(@NotNull final TypedQuery<E> query) {
        @NotNull final List<E> list = query.getResultList();
        if (list.isEmpty()) return Optional.empty();
        return Optional.of(list.get(0));
    }

    @Override
    public void rollback() {
        entityManager.getTransaction().rollback();
    }

    @Override
    public void begin() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commit() {
        entityManager.getTransaction().commit();
    }

    @Override
    public void close() {
        entityManager.close();
    }

}

