package ru.vpavlova.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.api.service.IActiveMQConnectionService;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;

@Getter
public class ActiveMQConnectionService implements IActiveMQConnectionService {

    private static ActiveMQConnectionService instance;

    @NotNull
    private static final String JMS_BROKER_URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    public static final String JMS_LOGGER_TOPIC = "JCG_TOPIC";

    @NotNull
    private final MessageService messageService;

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(JMS_BROKER_URL);

    @NotNull
    private final Connection connection;

    @SneakyThrows
    public ActiveMQConnectionService() {
        messageService = new MessageService();
        connection = connectionFactory.createConnection();
        connection.start();
        instance = this;
    }

    @Override
    @SneakyThrows
    public void shutDown() {
        connection.close();
    }

    public static ActiveMQConnectionService getInstance() {
        return instance;
    }

}
