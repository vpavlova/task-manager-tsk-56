package ru.vpavlova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.api.repository.dto.IProjectRepository;
import ru.vpavlova.tm.api.service.dto.IProjectService;
import ru.vpavlova.tm.dto.Project;
import ru.vpavlova.tm.enumerated.Sort;
import ru.vpavlova.tm.enumerated.Status;
import ru.vpavlova.tm.exception.empty.EmptyDescriptionException;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.empty.EmptyNameException;
import ru.vpavlova.tm.exception.empty.EmptyUserIdException;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.system.IndexIncorrectException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    public IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final Project entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.add(entity);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public Project add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.add(project);
            projectRepository.commit();
            return project;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Project> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            entities.forEach(projectRepository::add);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.clear();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Project entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneById(entity.getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            return projectRepository.findAll();
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            return projectRepository.findById(id);
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneById(id);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.clearByUserId(userId);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            return projectRepository.findAllByUserId(userId);
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            return projectRepository.findOneByIdAndUserId(userId, id);
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            return projectRepository.findOneByIndex(userId, index);
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Project> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            return projectRepository.findOneByName(userId, name);
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(
            @Nullable final String userId, @Nullable final Project entity
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneById(entity.getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByIdAndUserId(userId, id);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            @NotNull Optional<Project> project = projectRepository.findOneByIndex(userId, index);
            projectRepository.removeOneByIdAndUserId(userId, project.get().getId());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByName(userId, name);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable String userId, @Nullable String sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<Project> comparator = sortType.getComparator();
            return projectRepository
                    .findAllByUserId(userId)
                    .stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.clearByUserId(userId);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) return;
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        try {
            projectRepository.begin();
            projectRepository.update(entity.get());
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

}
